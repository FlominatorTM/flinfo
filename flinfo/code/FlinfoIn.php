<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoData.php');
require_once ('FlinfoHooks.php');
/**
 * Abstract base class for all input handlers.
 */
abstract class FlinfoIn {

	/**
	 * Constructor. The parameter file is supposed to be located in the Flinfo data directory
	 * as returned by {@link FlinfoData::dataDirectory()}; it may contain authorization information
	 * such as API keys.
	 *
	 * @param string $parameterFileName simple filename of the parameter file
	 * @param array  $requestParams     $_REQUEST
	 * @return Flinfo_In
	 */
	public abstract function __construct ($parameterFileName, $requestParams);

	/**
	 * Query the server about information for image ID $id.
	 * @param string $id           The ID or URL of the image
	 * @return array (string, int)
	 *   First element is the ID as determined by this function (possibly extracted from a URL $id).
	 *   Second element is the status code, can be FlinfoStatus::STATUS_OK, STATUS_SERVER_FAILURE,
	 *   STATUS_INTERNAL_ERROR, or STATUS_INVALID_ID.
	 */
	public abstract function getInfo ($id);

	/**
	 * Verify that the image does not belong to a blacklisted user
	 * @return boolean true if the user is not blacklisted, false otherwise
	 */
	public function verifyUser () {
		return true;
	}

	/**
	 * Extract and check the license info
	 * @param boolean $goodUser the result of verifyUser () above.
	 * @return array (string, array, string)
	 *   First element is null if the license is acceptable at the Commons, otherwise the name of the license.
	 *   Second element is an array of strings containing Commons license tags, without squiggly braces around them.
	 *   Third element is a string containing additional source information. May be null.
	 */
	public abstract function getLicenses ($goodUser);

	/**
	 * Get the review tag.
	 * @return String The plain review tag.
	 */
	protected abstract function internalGetReviewTag();

	/**
	 * Get the review tag. Calls the flinforeviewTag hooks.
	 * @return String The tag, without "{{" or "}}".
	 */
	public function getReviewTag () {
		$reviewTag = $this->internalGetReviewTag();
		$result = FlinfoHooks::run('flinfoReviewTag', array (&$reviewTag));
		if (is_string ($result)) {
			// TODO: Log, or raise an exception?
		}
		return $reviewTag;
	}

	/**
	 * Get Author information
	 * @return array (authors)
	 *   Each author itself is an array of three strings:
	 *   First element is a link to the author's page at the repository. May be null.
	 *   Second element is his screen name there.
	 *   Third element is the author's location, if known. Empty or null otherwise.
	 */
	public abstract function getAuthor ();

	/**
	 * Return a string containing the wikitext for the Author field. Useful
	 * if a template is to be supplied. If this function return null, a default author
	 * string is constructed using the result of getAuthor() instead. This default
	 * implementation return null.
	 *
	 * @return string
	 */
	public function getWikiAuthor () {
		return null;
	}

	/**
	 * Get the creation date of the image.
	 * @return int Time value as created by mktime.
	 */
	public abstract function getDate ();

	/**
	 * Return the strftime format for formatting the date.
	 * @return string
	 */
	public function getDateFormat () {
		return FlinfoData::msg('created');
	}

	/**
	 * Get the source information of the image.
	 * @return array (string, string)
	 *   First element is a link to the image description page at the repository.
	 *   Second element is the title of the image.
	 */
	public abstract function getSource ();

	/**
	 * Get the an alternate source link for the image, if any.
	 * @return string or null if none
	 */
	public function getAlternateSource () {
		return null;
	}

	/**
	 * Return a string containing the wikitext for the Source field. Useful
	 * if a template is to be supplied. If this function return null, a default source
	 * string is constructed using the result of getSource() instead. This default
	 * implementation return null.
	 *
	 * @return string
	 */
	public function getWikiSource () {
		return null;
	}

    /**
     * Return the name to be used for uploads to the Commons as the destination filename.
     * If null is returned, the image title as returned by GetSource() will be used. This
     * default implementation returns null.
     *
     * @return string
     */
    public function getUploadTitle () {
    	return null;
    }

	/**
	 * Get additional permission info as a string.
	 * @return array (String, String)
	 *     First element is a link to the permission statement.
	 *     Second element is a user name.
	 *     Either both are null or both are set.
	 */
	public function getPermission () {
		return array (null, null);
	}

	/**
	 * Return the geoinfo for the image
	 * @return array ('latitude' => x, 'longitude' => y, 'source' => Screen name of the reposiory)
	 */
	public function getGeoInfo () {
		return null;
	}

	/**
	 * Return the raw server response
	 * @return array
	 */
	public abstract function getRawResult ();

	/**
	 * Return the description of the image from the repository as a string
	 * @return string
	 */
	public abstract function getDescription ();

	/**
	 * Return additional wikitext to be put into the description.
	 * @return string
	 */
	public function getWikiDescription () {
		return null;
	}

	/**
	 * Return the title of the image from the repository as a string
	 * @return string
	 */
	public abstract function getTitle ();

	/**
	 * Return the repository-specific account ID on the account the image belongs to
	 * @return string
	 */
	public abstract function getAccountId ();

	/**
	 * Return the available sizes of the image from the repository
	 * @return array (array ())
	 *   An array of arrays, each nested array has the format
	 *   array ('width' => x, 'height' => y, 'source' => url)
	 */
	public abstract function getSizes ();

	/**
	 * Return a list of possible categories as reported by the repository. The
	 * function should not attempt to pare down the list by looking for sensible
	 * categories at the Commons; that's the caller's job. Just return whatever
	 * meaningful tags or categories the image has at the server.
	 *
	 * @return array
	 *   An array of strings, each giving a potential category name.
	 */
	public function getCategories () {
		return array ();
	}

	/**
	 * Called after categories have been reolved. May remove some categories, or otherwise
	 * postprocess categories. This default implementation returns the argument unchanged.
	 *
	 * @param array $categories Array of strings, containing valid and resolved category
	 *                          names, including the "Category:" prefix.
	 * @return array Filtered list of categories to propose.
	 */
	public function filterCategories ($categories) {
		return $categories;
	}

	protected $mServerError = null;

	/**
	 * Get a server error message (if any). Returns null if no server error.
	 * @return string A possibly empty server error message
	 */
	public function getServerError () {
		return $this->mServerError;
	}

	/**
	 * Set the server error message.
	 * @param string $msg
	 * @return void
	 */
	protected function setServerError ($msg) {
		if (is_string ($msg)) {
			$this->mServerError = $msg;
		} else {
			$this->mServerError = null;
		}
	}

	/**
	 * Load a parameter file from the globally defined parameter directory
	 * {@link FlinfoData::dataDirectory()}.
	 *
	 * @param string $name The file name (relative to {@link FlinfoData::dataDirectory()}) of the parameter file.
	 * @return string
	 *    The file's contents as a string as returned by {@link file_get_contents}.
	 */
	protected function getParamFile ($name) {
		$dataDir = FlinfoData::dataDirectory();
		return file_get_contents ("$dataDir/$name");
	}

	/**
	 * Load a parameter file through {@link FlinfoIn::getParamFile()} and parses it according to the syntax below into an
	 * associative array of key-value pairs.
	 * <p>
	 * Syntax of each line: <code>key {"=" key}* "=" value</code>
	 * 
	 * @param string $name
	 * @return array
	 * 	  The file parsed into an associative array of key-value pairs.
	 */
	protected function loadParamFile ($name) {
		$result = array ();
		$data = $this->getParamFile ($name);
		if ($data) {
			$lines = explode ("\n", $data);
			foreach ($lines as $line) {
				$vals = explode('=', $line);
				$length = count($vals);
				if ($length < 2) continue;
				$value = trim($vals[$length - 1]);
				for ($i = 0; $i < $length - 1; $i++) {
					$result[trim($vals[$i])] = $value;
				}
			}
		}
		return $result;
	}
	
	/**
	 * Convert a string representation of date and time YYYY-MM-DD HH:MM:SS into
	 * a time value.
	 * @param string $date Date string YYYY-MM-DD HH:MM:SS
	 * @return int Time value as returned by mktime (i.e. seconds since 1970-01-01 00:00:00)
	 */
	protected function convertISODate ($date) {
		$date = str_replace('-', ' ', $date);
		$date = str_replace(':', ' ', $date);
		$parts = explode(' ', $date);
		//Result is an array with 6 elements Y|M|D|h|m|s
		return mktime($parts[3], $parts[4], $parts[5], $parts[1], $parts[2], $parts[0]);
	}

	/**
	 * Given a creativecommons.org license URL, extract the license tag.
	 *
	 * @param string $url creativecommons.org URL
	 * @return string License tag, or null if not a Commons-compatible license.
	 */
	public static function ccLicenseFromUrl ($url, $author = null) {
		$license = null;
		if (preg_match ('!^(?:https?:)?//creativecommons\.org/licenses/by/([0-9.]+)(/([-a-z]+))?/?!', $url, $matches)) {
			$license = 'cc-by-' . $matches[1];
			if (count ($matches) > 3 && $matches[3] && $matches[3] != '') {
				$license .= '-' . $matches[3]; // Localized version of the license
			}
		} else if (preg_match ('!^(?:https?:)?//creativecommons\.org/licenses/by-sa/([0-9.]+)(/([-a-z]+))?/?!', $url, $matches)) {
			$license = 'cc-by-sa-' . $matches[1];
			if (count ($matches) > 3 && $matches[3] && $matches[3] != '') {
				$license .= '-' . $matches[3]; // Localized version of the license
			}
		} else if (preg_match ('!^(?:https?:)?//creativecommons\.org/publicdomain/zero/([0-9.]+)/?!', $url, $matches)) {
			$license = 'cc-zero';
		} else if (preg_match('!^(?:https?:)?//creativecommons\.org/publicdomain/mark/([0-9.]+)/?!', $url, $matches)) {
			$license = 'subst:Flickr-public domain mark/subst';
		} else if ($author !== null && preg_match('!^(?:https?:)?//([^.]+\.)?creativecommons\.org/[Pp]ublic(%20|_)?domain/?!', $url, $matches)) {
			$license = 'pd-author|1=' . FlinfoData::replaceBrackets($author);
		}
		return $license;
	}
}