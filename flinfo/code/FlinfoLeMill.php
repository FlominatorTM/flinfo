<?php
/**
 * Flinfo
 *
 * Copyright (C) 2012 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Flinfo input handler for lemill.net
 */
require_once ('FlinfoData.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');

require_once ('lib/Curly.php');

/**
 * Input handler for www.mushroomobserver.org. Uses screenscraping.
 */
class FlinfoLeMill extends FlinfoIn {

	private $mRawId = null;
	private $mStatus = null;
	private $mId = null;
	private $mSizes = null;
	private $mUserId = null;
	private $mUserName = null;
	private $mUserUrl = null;
	private $mTitle = null;
	private $mDescription = null;
	private $mDescUrl = null;
	private $mLicense = null;
	private $mLicenseUrl = null;
	private $mDate = 0;

	public function __construct ($parameterFileName, $requestParams) {
		// Nothing to do.
	}

	private function extractId ($rawId) {
	    if (preg_match ('!^http://(?:www\.)?lemill\.net/content/pieces/([^/]+)!', $rawId, $matches)) {
	    	return $matches[1];
	    } else {
			// Otherwise, assume it *is* the id
			return $rawId;
		}
	}

	public function getInfo ($id) {
		$this->mRawId = $id;
		$this->mId = $this->extractId ($id);
		if ($this->mId === null) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($id, $this->mStatus);
		}
		$this->mDescUrl = "http://lemill.net/content/pieces/" . $this->mId;
		$info = Curly::getContents ($this->mDescUrl);
		if (!$info) {
			$this->mStatus = FlinfoStatus::STATUS_SERVER_FAILURE;
			return array ($id, $this->mStatus);
		}
		// Sanity check
		if (!preg_match ('!<a\s+href="http://lemill.net/content/pieces/' . preg_quote ($this->mId, '!') . '[^"]*"!', $info)) {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
			return array ($this->mDescUrl, $this->mStatus);
		}
		// Extract the info from the HTML (Screenscraping)
		$imgPos = -1;
		if (preg_match ('!<img\s+src="http://lemill.net/content/pieces/' . preg_quote ($this->mId, '!') . '[^"]*"[^>]*class="media_piece"[^>]*title="([^"]*)"!', $info, $matches, PREG_OFFSET_CAPTURE)) {
			$imgPos = $matches[0][1];
			$this->mTitle = trim (html_entity_decode($matches[1][0]));
			// No size info, but we know that we have
			// ... /coverImage a 120px (width or height) thumbnail
			// ... /at_download/file full size
			$this->mSizes = array ();
			// TODO: get the file and compute the true sizes...
			$this->mSizes[]  = array ('width' => 120, 'height' => 120, 'source' => $this->mDescUrl . '/coverImage');
			$this->mSizes[]  = array ('width' => -1, 'height' => -1, 'source' => $this->mDescUrl . '/at_download/file');
		}
		if (preg_match ('!<div\s+class="documentByLine"[^<]+<a\s+href="(http://lemill.net/community/people/([^"]+))"[^>]*>([^<]+)</a>(?:[^<]*((\d\d\d\d)-(\d\d)-(\d\d)\s(\d\d):(\d\d)))?!', $info, $matches)) {
			$this->mUserName = trim(html_entity_decode($matches[3]));
			$this->mUserUrl = html_entity_decode($matches[1]);
			$this->mUserId = html_entity_decode($matches[2]);
			if (count ($matches) > 3 && $matches[4]) {
				$this->mRawDate = $matches[4];
				$this->mDate = mktime($matches[8], $matches[9], 0, $matches[6], $matches[7], $matches[5]);
			}
		}
		// Description
		$this->mDescription = '';
		if (preg_match ('!<img\s+src="http://lemill.net/content/pieces/' . preg_quote ($this->mId, '!') . '(\s|\S)*?<div>((\s|\S)*?)</div>!', $info, $matches, 0, $imgPos)) {
			$desc = $matches[2];
			// Strip initial "<br /> and label tags
			if (preg_match ('!^((?:<br />|<label>(?:\s|\S)*?</label>|\s)*)!', $desc, $matches)) {
				$desc = trim (html_entity_decode (substr ($desc, strlen ($matches[0]))));
				$lines = explode("\n", $desc);
				$newLines = array();
				foreach ($lines as $line) {
					$l = trim ($line);
					$l = str_replace (array ('<p>', '</p>'), array ("", "\n"), $l);
					$l = str_replace (array ('<br />', '<br/>'), array ("\n", "\n"), $l);
					if (strlen($l) > 0) $newLines[] = trim($l);
				}
				$lines = implode ("\n", $newLines);
				$this->mDescription = $lines;
			}
		}
		if (preg_match ('!<a\s+rel="license"\s+href="([^"]*)"\s*>!', $info, $matches)) {
			$this->mLicenseUrl = $matches[1];
			$this->mLicense = self::ccLicenseFromUrl ($this->mLicenseUrl);
		}
		// Check that we have all required fields
		if ($this->mLicense && $this->mUserId && $this->mSizes) {
			$this->mStatus = FlinfoStatus::STATUS_OK;
		} else {
			$this->mStatus = FlinfoStatus::STATUS_INVALID_ID;
		}
		return array ($this->mDescUrl, $this->mStatus);
	}

	public function getAccountId () {
		return $this->mUserId;
	}

	public function getAuthor () {
		return array (array ($this->mUserUrl, $this->mUserName, null));
	}

	public function getSource () {
		return array ('http://lemill.net/content/pieces/' . $this->mId, $this->getTitle());
	}

	public function getLicenses ($goodUser) {
		$tags = array ();
		$status = $this->mLicenseUrl ? $this->mLicenseUrl : 'Unknown License';
		if ($goodUser) {
			if ($this->mLicense) {
				$tags[] = $this->mLicense;
				$status = null;
				$tags[] = $this->getReviewTag();
			}
		}
		return array ($status, $tags, null);
	}

	protected function internalGetReviewTag () {
		return "lemillreview";
	}

	public function getTitle () {
		return $this->mTitle;
	}

	public function getDescription () {
		return $this->mDescription;
	}

	public function getDate () {
		return $this->mDate;
	}

	public function getSizes () {
		return $this->mSizes;
	}

	/**
	 * Fake a JSON server result and return that.
	 *
	 * @return Faked "raw" server result.
	 */
	public function getRawResult () {
		if ($this->mStatus === null) return null;
		$result = array ("status" => $this->mStatus, "raw_id" => $this->mRawId);
		$photo = null;
		if ($this->mId !== null) {
			$photo = array ();
			$photo["id"] = $this->mId;
		}
		if ($this->mStatus == FlinfoStatus::STATUS_OK) {
			$photo["url"] = $this->mDescUrl;
			$photo["title"] = $this->mTitle;
			$photo["owner"] = array ("userId" => $this->mUserId, "userName" => $this->mUserName, "userPage" => $this->mUserUrl);
			$photo["description"] = $this->mDescription;
			$photo["license"] = array ("key" => $this->mLicense, "link" => $this->mLicenseUrl);
			if ($this->mDate) $photo["date"] = $this->mRawDate;
			$photo["sizes"] = $this->mSizes;
		}
		if ($photo) $result["photo"] = $photo;
		return $result;
	}

}