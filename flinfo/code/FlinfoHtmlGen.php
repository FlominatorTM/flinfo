<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * A very basic set of utility routines to generate HTML output. The main benefit of using this
 * is twofold:
 *
 * - It can make the source code easier to read.
 * - The routines automatically correctly escape attributes and plain text input, which helps
 *   avoiding XSS vulnerabilities.
 *
 * Assumes UTF-8 encoding, as it only will HTML-encode &, <, >, ', and ".
 */
abstract class FlinfoHtmlGen {
	
	/**
	 * Escape text to be inserted in HTML.
	 *
	 * @param string $literal Text to be inserted.
	 * @param boolean $forAttr if true, also escape single and double quotes. Defaults to false.
	 * @return HTML-escaped string.
	 */
	private static function makeSafe ($literal, $forAttr = false) {
		$result = str_replace ('&', '&amp;', $literal);
		$result = str_replace ('<', '&lt;', $result);
		$result = str_replace ('>', '&gt;', $result);
		if ($forAttr) {
			$result = str_replace ("'", '&apos;', $result);
			$result = str_replace ('"', '&quot;', $result);
		}
		return $result;
	}
	
	/**
	 * Get a DOCTYPE string.
	 *
	 * @return string The DOCTYPE
	 */
	public static function docType () {
		return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
	}
	
	/**
	 * Create an opening tag for the $element with the given attributes, escaping attribute values correctly.
	 * If $isSingle == true, close the tag with " />". The element name and attribute key are not checked!
	 *
	 * @param string $element Tag name
	 * @param array attributes Associative array of tag attributes.
	 * @param boolean $isSingle (optional) if true, close the tag with " />", otherwise just with ">".
	 *
	 * @return string the HTML tag
	 */
	public static function openElement ($element, $attributes, $isSingle = false) {
		$html = "<$element";
		if (is_array ($attributes)) {
			foreach ($attributes as $k => $v) {
				$html .= " $k='" . self::makeSafe ('' . $v, true) . "'";
			}
		}
		$html .= ($isSingle ? ' />' : '>');
		return $html;
	}
	
	/**
	 * Generate a closing tag for $element.
	 *
	 * @return string "</$element>"
	 */
	public static function closeElement ($element) {
		return "</$element>";
	}
	
	/**
	 * Properly escape HTML text.
	 *
	 * @return string The input text with all "&" replaced by "&amp;" and all "<" replaced by "&lt;"
	 */
	public static function text ($text) {
		return self::makeSafe ($text);
	}
	
	/**
	 * Create a single element (immediately closed with " />") with the given attributes.
	 *
	 * @return string The HTML tag
	 */
	public static function singleElement ($element, $attributes = null) {
		return self::openElement ($element, $attributes, true);
	}
	
	/**
	 * Open the given element with the given attributes, write the text $content, and the closing tag.
	 *
	 * @return string The HTML for the element.
	 */
	public static function element ($element, $attributes, $content) {
		$html = self::openElement ($element, $attributes);
		if ($content) $html .= self::makeSafe ($content);
		$html .= self::closeElement ($element);
		return $html;
	}
	
	/**
	 * Open the specified element, without attributes.
	 *
	 * @return string "<$element>"
	 */
	public static function open ($element) {
		return self::openElement ($element, null, false);
	}
	
	/**
	 * Generate a closing tag for the given element.
	 *
	 * @return string "</$element>"
	 */
	public static function close ($element) {
		return self::closeElement ($element);
	}
	
	/**
	 * Convenience routine to be called from within an open table cell. Generates HTML to close the
	 * cell and its row, and then opens the next row and its first cell.
	 *
	 * @param array $attributes Attributes for the newly opened cell
	 * @return "</td></tr><tr><td>"
	 */
	public static function newRow ($attributes = null) {
		return self::closeElement('td')
		     . self::closeElement('tr')
			 . self::open ('tr')
			 . self::openElement ('td', $attributes);
	}
	
	/**
	 * Convenience method to be called from within an open table cell. Generates HTML to close the cell
	 * and open the next one.
	 *
	 * @param array $attributes Attributes for the newly opened cell
	 * @return "</td><td>"
	 */
	public static function newCell ($attributes = null) {
		return self::closeElement('td')
			 . self::openElement ('td', $attributes);
	}
	
	/**
	 * Convenience method to open a script tag.
	 *
	 * @return HTMl opening code for a script tag.
	 */ 
	public static function openInlineScript () {
		return self::openElement ('script', array ('type' => 'text/javascript')) . "/*<![CDATA[*/\n";
	}
	
	/**
	 * Convenience method to close a script tag.
	 *
	 * @return HTMl closing code for a script tag.
	 */ 
	public static function closeInlineScript () {
		return "/*]]>*/" . self::closeElement ('script');
	}
}
