﻿<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2011 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Flinfo input handler for Landesarchiv Baden-Württemberg
 */
require_once ('FlinfoData.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');

require_once ('lib/Curly.php');

/**
 * Input handler for Landesarchiv Baden-Württemberg. Uses screenscraping.
 */
class FlinfoLABW extends FlinfoIn {

	const CREATOR_PRAGHER = "Willy Pragher";
	private $mBestand = null;
	private $mId = null;
	private $mFileName = null;
	// private $mRawId = null;
	private $mStatus = null;
	
	// private $mId = null;
	private $mSizes = null;
	// private $mUserId = null;
	// private $mUserName = null;
	// private $mUserUrl = null;
	//private $mTitle = null;
	 private $mDescription = null;
	 private $mPermalink = null;
	 private $mArchiveId = null;
	 private $mArtist = null;
	// private $mLicenseUrl = null;
	// private $mRawDate = null;
	private $mDate = null;
	private $mDescUrl = null;

	public function __construct ($parameterFileName, $requestParams) {
		// FlinfoHooks::register ('flinfoHtmlAfterTextArea', array ($this, 'htmlHook'));
	}

	public function getInfo ($id) {
		$this->mStatus = FlinfoStatus::STATUS_MISSING_ID;

		if(stristr($id, 'plink'))
		{
			$id = $this->getUrlFromPermalink($id);
		}
		$parts = parse_url($id);
		
		if(isset($parts['query']))
		{
			parse_str($parts['query'], $query);
			
			if(isset($query['sprungId'])&& isset($query['bestand']))
			{
				$this->getInfoZoomFirst($query['sprungId'], $query['bestand']);
			}
			else if(isset($query['id']) && isset($query['bestand']))
			{
				$this->getInfoZoomFirst($query['id'], $query['bestand']);
			}
			else if (isset($query['setzeOlfLesezeichen']) && isset($query['datei']))
			{
				$this->getInfoDescriptionFirst($query['setzeOlfLesezeichen'], $query['datei']);
			}
		}
		return array ($this->mPermalink, $this->mStatus);
	}

	private function getInfoZoomFirst($id, $bestand)
	{
		$this->mBestand = $bestand;
		$this->mId = $id;
		
		$zoomPage = $this->getZoomPageContents();
		$this->extractZoomPageContents($zoomPage);
		
		$detailPage = $this->getDetailPageContents();
		$this->extractDetailPageContents($detailPage);
		
		$this->mStatus = FlinfoStatus::STATUS_OK;
	}
	
	private function getInfoDescriptionFirst($id, $filename)
	{
		//a bookmark link without 'bestand' requires a different approach
		$this->mFileName = $filename;
		$this->mId = $id;
		
		$detailPage = $this->getDetailPageContents();
		$this->mBestand = $this->extractFromUntil($detailPage, 'bestand=', '&');
		
		if($this->mBestand)
		{
			$this->extractDetailPageContents($detailPage);
			
			//zoom page is needed for picture links and size
			$zoomPage = $this->getZoomPageContents();
			$this->extractZoomPageContents($zoomPage);
			$this->mStatus = FlinfoStatus::STATUS_OK;
		}
	}
	
	private function getUrlFromPermalink($url)
	{
		//one of those two formats
		//http://www.landesarchiv-bw.de/plink/?f=5-130529
		//https://www2.landesarchiv-bw.de/ofs21/plink.php?f=
		$parts = parse_url($url);
		
		if(isset($parts['query']))
		{
			parse_str($parts['query'], $query);
			if(isset($query['f']))
			{
				$ww2RedirUrl = "https://www2.landesarchiv-bw.de/ofs21/plink.php?f=".$query['f'];
				$context = stream_context_create(
					array(
						'http' => array(
							'follow_location' => false
						)
					)
				);

				$html = file_get_contents($ww2RedirUrl, false, $context);
				foreach($http_response_header as $headerPart)
				{
					if(stristr($headerPart, "Location:"))
					{
						return $headerPart;
					}
				}
			}
		}
	}
	//show zoom page with image zoomed to 100%
	private function getZoomPageContents()
	{
		//via http://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
		$url = 'https://www2.landesarchiv-bw.de/ofs21/bild_zoom/zoom.php';
		$data = array(	'bestand' => $this->mBestand, 
						'id' => $this->mId,
						'zoomgewaelt' => '1',
						'zoom' => '100');

		//use key 'http' even if you send the request to https://...
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		if ($result === FALSE) 
		{ 
			$this->mStatus = FlinfoStatus::STATUS_SERVER_FAILURE;
			return false;
		}

		return $result;
	}
	private function extractZoomPageContents($info)
	{
		$filename=$this->extractFromUntil($info, '&datei=', '&extern=1');
		if($filename)
		{
			$this->mFileName = $filename;
			
			$pictureDivBox = $this->extractFromUntil($info, '<div id="zoombild"', '</div>');
			if($pictureDivBox)
			{
				$thisW =  $this->extractFromUntil($pictureDivBox, 'height="', '"');
				$thisH =   $this->extractFromUntil($pictureDivBox, 'width="', '"');
				$thisS = $this->extractFromUntil($pictureDivBox, 'src="', '"');
				$this->mSizes[] = array ("width" => $thisW, "height" => $thisH, "source" => 'https://www2.landesarchiv-bw.de/ofs21/bild_zoom/' . $thisS .  '&jpegQualitaetZoombild=95');
			}
			else
			{
				$this->mStatus = FlinfoStatus::STATUS_MISSING_ID;
			}
		}
		else
		{
			$this->mStatus = FlinfoStatus::STATUS_MISSING_ID;
		}
		
	}
	
	private function extractFromUntil($haystack, $needle, $end)
	{
		$indexOfNeedle = strpos($haystack, $needle);
		if($indexOfNeedle>0)
		{
			$indexOfNeedle+=strlen($needle);
			$indexOfEnd = strpos($haystack, $end, $indexOfNeedle);
			$length = $indexOfEnd - $indexOfNeedle;
			return substr($haystack, $indexOfNeedle, $length);
		}
		else
		{
			return false;
		}
	}

	private function getDetailPageContents()
	{
		$this->mDescUrl = "https://www2.landesarchiv-bw.de/ofs21/lesezeichen/index.php?notizen%5B". $this->mId . "_1%5D=&setzeOlfLesezeichen=".$this->mId."&seite=Bild+1&datei=".$this->mFileName."&extern=1&lesezeichenAusfuehrlich=1";
		return Curly::getContents ($this->mDescUrl);
	}
	private function getNextValueClean($descPieces, $i)
	{
		return trim(strip_tags('<td '.$descPieces[$i+1]));
	}
	private function extractDetailPageContents($info)
	{
		$descPieces = explode('<td', $info);
		for($i=0;$i<count($descPieces);$i++)
		{
			if(stristr($descPieces[$i], 'Titel:'))
			{
				$this->mDescription.= $this->getNextValueClean($descPieces, $i);
			}
			else if(stristr($descPieces[$i], 'Autor/Fotograf/Künstler:'))
			{
				$this->mArtist = $this->getNextValueClean($descPieces, $i);
			}
			else if(stristr($descPieces[$i], 'Aus Bestand:'))
			{
				$br2nl = str_replace('<br>', "\n", $descPieces[$i+1]);
				$this->mDescription.= strip_tags('<td '. $br2nl ). "\n";
			}
			else if(stristr($descPieces[$i], 'Laufzeit:'))
			{
				$germanDateString = $this->getNextValueClean($descPieces, $i);
				$german = array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
				$english = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
				$dateString = str_replace('.', '', str_ireplace($german, $english, $germanDateString));
				
				$this->mDate = strtotime($dateString);
			}
			else if(stristr($descPieces[$i], 'Permalink:'))
			{
				$this->mPermalink = $this->getNextValueClean($descPieces, $i);
			}
			else if(stristr($descPieces[$i], 'Archivalieneinheit')) 
			{
				$this->mArchiveId = str_replace(' - Archivalieneinheit', '', $this->getNextValueClean($descPieces, $i-1));
			}
		}
	}

	public function getAccountId () {
		return "getAccountId";
	}

	public function getAuthor () {
		return array (array ("UserUrl", "UserName", null));
	}

	public function getWikiAuthor () {
		if(stristr($this->mArtist,self::CREATOR_PRAGHER))
		{
			return '{{Creator:Willy Pragher}}';
		}
		else
		{
			return $this->mArtist;
		}
	}

	public function getSource () {
		return array ($this->mDescUrl, null);
	}

	public function getWikiSource () {
		return '[' . $this->mPermalink . ' ' . $this->mArchiveId.']';
	}

	public function getLicenses ($goodUser) {
		$tags = array ();
		$status = null; 
		$tags[] = "cc-by-3.0-de|Landesarchiv Baden-Württemberg, Fotograf: ". $this->mArtist;
		$tags[] = "subst:chc"; //check categories with timestamp, because Landesarchiv doesn't provide any tags at the moment, so we can only add the photographer's 
		return array ($status, $tags, null);
	}

	protected function internalGetReviewTag () {
		return "licensereview";
	}

	public function getTitle () {
		return null;
	}

	public function getDescription () {
		return $this->mDescription;
	}

	public function getCategories () {
		$tags = array ();
		$tags[] = "Photographs by ‎" . $this->mArtist;
		return $tags;
	}

	public function getDate () {
		return $this->mDate;
	}

	public function getDateFormat () {
		//return "%e. %B %Y";
		 return "%Y-%m-%d";
	}
	
	public function getPermission () {
		return array ("http://www.landesarchiv-bw.de/web/54580", "Nutzungsbedingungen für Inhalte und Digitalisate auf den Webseiten des Landesarchivs");
	}

	public function getSizes () {
		return $this->mSizes;
	}

	/**
	 * Fake a JSON server result and return that.
	 *
	 * @return Faked "raw" server result.
	 */
	public function getRawResult () {
		if ($this->mStatus === null) return null;
		$result = array ("status" => "this->mStatus", "raw_id" => "this->mRawId");
		$photo = null;
		if (true || $this->mId !== null) {
			$photo = array ();
			$photo["id"] = "this->mId";
		}
		if (true || $this->mStatus == FlinfoStatus::STATUS_OK) {
			 $photo["url"] = "this->mDescUrl";
			// $photo["title"] = "this->mTitle";
			// $photo["owner"] = array ("userId" => "this->mUserId", "userName" => "this->mUserName", "userPage" => "this->mUserUrl");
			// $photo["license"] = array ("key" => "this->mLicense", "link" => "this->mLicenseUrl");
			//if ($this->mDate) $photo["date"] = "this->mRawDate";
			$photo["sizes"] = "this->mSizes";
			// if (true || $this->mGeo) $photo["geo"] = "this->mGeo";
		}
		if ($photo) $result["photo"] = $photo;
		return $result;
	}

}